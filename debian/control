Source: qcom-support
Section: metapackages
Priority: optional
Maintainer: Arnaud Ferraris <aferraris@debian.org>
Standards-Version: 4.6.2
Build-Depends: debhelper-compat (= 13)
Homepage: https://salsa.debian.org/Mobian-team/devices/qcom-support
Vcs-Git: https://salsa.debian.org/Mobian-team/devices/qcom-support.git
Vcs-Browser: https://salsa.debian.org/Mobian-team/devices/qcom-support
Rules-Requires-Root: no

Package: qcom-support-common
Architecture: all
Depends: ${misc:Depends},
         droid-juicer,
         firmware-atheros,
         firmware-qcom-soc,
         libdrm-freedreno1,
         libqmi-utils,
         make-dynpart-mappings,
         mobian-tweaks-common,
         mobile-usb-networking,
         protection-domain-mapper,
         qcom-phone-utils,
         qrtr-tools,
         rmtfs,
         tqftpserv,
Description: Hardware support packages for Qualcomm-based devices
 This metapackage pulls in all common packages needed to run on devices
 based on Qualcomm SoC's.

Package: sdm845-support
Architecture: all
Depends: ${misc:Depends},
         qcom-support,
Description: Hardware support for SDM845 devices - transitional package
 This is a transitional package. It can safely be removed.

Package: qcom-support
Architecture: all
Depends: ${misc:Depends},
         linux-image-6.12-qcom,
         qcom-support-common,
Breaks: sdm845-support (<< 0.3.0~),
        sm7225-support (<< 0.3.0~),
Replaces: sdm845-support (<< 0.3.0~),
          sm7225-support (<< 0.3.0~),
Provides: sdm845-support,
          sm7225-support,
Description: Hardware support packages for Qualcomm-based devices
 This metapackage pulls in all packages needed to run on devices based
 on the Qualcomm SoC's currently supported in Mobian.

Package: sm7225-support
Architecture: all
Depends: ${misc:Depends},
         qcom-support,
Description: Hardware support for SM7225 devices - transitional package
 This is a transitional package. It can safely be removed.

Package: sdm670-support
Architecture: all
Depends: ${misc:Depends},
         linux-image-6.12-sdm670,
         qcom-support-common,
Description: Hardware support for SDM670 devices
 This metapackage pulls in all common packages needed to run on qcom devices
 plus the specific kernel supporting devices equipped with SDM670 SoC.

Package: sc7280-support
Architecture: all
Depends: ${misc:Depends},
         linux-image-6.12-sc7280,
         qcom-support-common,
Description: Hardware support for SC7280 devices
 This metapackage pulls in all common packages needed to run on qcom devices
 plus the specific kernel supporting devices equipped with SC7280 SoC.
